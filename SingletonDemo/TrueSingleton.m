//
//  TrueSingleton.m
//  SingletonDemo
//
//  Created by wx on 2/2/16.
//  Copyright © 2016 wx. All rights reserved.
//

#import "TrueSingleton.h"

@implementation TrueSingleton
static TrueSingleton* _instance;

+ (instancetype)allocWithZone:(struct _NSZone*)zone
{
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _instance = [super allocWithZone:zone];
  });
  return _instance;
}

+ (instancetype)sharedInstance
{
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _instance = [[self alloc] init];
  });
  return _instance;
}

- (id)copyWithZone:(NSZone*)zone
{
  return _instance;
}

@end
