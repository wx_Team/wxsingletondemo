//
//  TestSingleton.m
//  SingletonDemo
//
//  Created by wx on 1/5/16.
//  Copyright © 2016 wx. All rights reserved.
//

#import "TestSingleton.h"

@implementation TestSingleton
static TestSingleton* _instance;

+ (TestSingleton*)defaultInstance
{
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _instance = [[TestSingleton alloc] init];
  });

  return _instance;
}

@end
