//
//  TestSingleton.h
//  SingletonDemo
//
//  Created by wx on 1/5/16.
//  Copyright © 2016 wx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TestSingleton : NSObject
+ (TestSingleton*)defaultInstance;
@end
