//
//  TrueSingleton.h
//  SingletonDemo
//
//  Created by wx on 2/2/16.
//  Copyright © 2016 wx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrueSingleton : NSObject
+ (instancetype)sharedInstance;
@end
