//
//  ViewController.m
//  SingletonDemo
//
//  Created by wx on 1/5/16.
//  Copyright © 2016 wx. All rights reserved.
//

#import "TestSingleton.h"
#import "TrueSingleton.h"
#import "ViewController.h"

@interface
ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  TestSingleton* singleton1 = [[TestSingleton alloc] init];
  TestSingleton* singleton2 = [TestSingleton defaultInstance];
  TestSingleton* singleton3 = [[TestSingleton alloc] init];

  TrueSingleton* trueSingleton1 = [[TrueSingleton alloc] init];
  TrueSingleton* trueSingleton2 = [TrueSingleton sharedInstance];
  TrueSingleton* trueSingleton3 = [[TrueSingleton alloc] init];
  // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
