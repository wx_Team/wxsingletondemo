//
//  main.m
//  SingletonDemo
//
//  Created by wx on 1/5/16.
//  Copyright © 2016 wx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
